import axios from 'axios';
import { push, replace } from 'react-router-redux';
import baseUrl from '../services/baseUrl';

export const getVideos = () => {
  const request = axios.get(baseUrl.API_URL);
  return {
    type: 'VIDEOS_FETCHED',
    payload: request,
  };
};
export const salveLocalStorage = video => ({
  type: 'LIST_CHANGED',
  listVideo: video,
});

export const showHistory = (show) => {
  return {
    type: 'HISTORY_SHOW',
    listVideo: show,
  };
};

export const redirect = video => (dispatch) => {
  dispatch(push(`/video/${video}`));
};
