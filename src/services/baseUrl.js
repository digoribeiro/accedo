const urlDevelopment = 'http://localhost:5000/videos';
const urlProduction = 'http://accedo-teste-api-tv.umbler.net/videos';
const API_URL =
  process.env.NODE_ENV === 'production' ? urlProduction : urlDevelopment;

export default {
  API_URL: API_URL,
};
