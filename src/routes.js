import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from 'views/Home';
import MainHeader from './components/MainHeader';
import SingleVideo from './views/SingleVideo';

/* eslint-disable react/prefer-stateless-function */

class Routes extends React.Component {
  render() {
    return (
      <span>
        <MainHeader />
        <main className="wrap container-fluid">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/video/:videoId" component={SingleVideo} />
          </Switch>
        </main>
      </span>
    );
  }
}

export default Routes;
