import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { showHistory } from '../actions/videos';
import History from './History';

const MainHeader = props => (
  <header className="header">
    <div className="wrap container-fluid">
      <div className="row middle-md content-header">
        <h1 className="col-md-6 brand">
          <NavLink to="/">Home</NavLink>
        </h1>
        <div className="col-md-6 text-right">
          <a
            onKeyPress={e => props.showHistory(e)}
            onClick={e => props.showHistory(e)}
            role="button"
            tabIndex="-1"
            className="link"
          >
            History
          </a>
        </div>
      </div>
    </div>
    <History />
  </header>
);
export default connect(
  state => ({
    showVideoHistory: state.video.showVideoHistory,
  }),
  {
    showHistory,
  },
)(MainHeader);
