import React, { Component } from 'react';

class Player extends Component {
  componentDidMount() {
    document.addEventListener('keyup', this.handleKeyup);
    document
      .getElementById('Player')
      .addEventListener('ended', this.handleEnded);
  }
  handleKeyup(e) {
    const player = document.getElementById('Player');
    switch (e.keyCode) {
      case 27:
        history.back();
        break;
      case 32:
        if (player.paused) {
          player.play();
        } else {
          player.pause();
        }
        break;
      case 37:
        player.currentTime += -30;
        break;
      case 39:
        player.currentTime += 30;
        break;
      default:
    }
  }

  handleEnded(e) {
    history.back();
  }

  render() {
    return (
      <div id="video-player" className="play-video row">
        <h3 className="col-xs-2">{this.props.title}</h3>
        <ul className="player-legend col-xs-6">
          <li>
            Esc <small>( Back to home ) </small>
          </li>
          <li>
            Space or Enter <small>( Pause and play ) </small>
          </li>
          <li>
            Left <small>( Back 30 seconds ) </small>
          </li>
          <li>
            Right <small>( Forward 30 seconds ) </small>
          </li>
        </ul>
        <video id="Player" src={this.props.videoUrl} controls autoPlay />
      </div>
    );
  }
}

export default Player;
