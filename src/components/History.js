import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import Video from './Video';

class History extends Component {
  renderVideosHistory() {
    const { listVideo } = this.props;
    const videos =
      typeof listVideo === 'object' ? listVideo : JSON.parse(listVideo);
    return videos.map(video => (
      <NavLink to={`/video/${video.id}`} key={video.id}>
        <Video
          key={video.id}
          className="video-history"
          image={video.image}
          title={video.title}
        />
      </NavLink>
    ));
  }
  render() {
    const { showVideoHistory } = this.props;
    return (
      <div className={`all-videos-history ${showVideoHistory ? 'hide' : ''}`}>
        {this.renderVideosHistory()}
      </div>
    );
  }
}

export default connect(
  state => ({
    listVideo: state.video.listVideo,
    showVideoHistory: state.video.showVideoHistory,
  }),
  {},
)(History);
