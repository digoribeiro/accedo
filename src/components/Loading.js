import React from 'react';

const Loading = props => (
  <div className={`loading-bg ${props.isShow}`}>
    <div className="spinner">
      <div className="double-bounce1" />
      <div className="double-bounce2" />
    </div>
  </div>
);

export default Loading;
