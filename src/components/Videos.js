import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getVideos, salveLocalStorage, redirect } from '../actions/videos';
import Video from './Video';
import Loading from './Loading';

class Videos extends Component {
  constructor(props) {
    super(props);
    this.addVideoHistory = this.addVideoHistory.bind(this);
    this.state = { listVideos: this.props.listVideo };
  }
  componentWillMount() {
    this.props.getVideos();
  }
  redirectVideoPage(obj) {
    this.props.redirect(obj.id);
  }
  addVideoHistory(e, obj) {
    const state = this.state.listVideos;
    for (let i = 0; i < state.length; i += 1) {
      if (state[i].id === obj.id) {
        this.redirectVideoPage(obj);
        return true;
      }
    }
    this.setState(
      {
        listVideos: [...this.state.listVideos, obj,]
      },
      () => {
        this.redirectVideoPage(obj);
        this.props.salveLocalStorage(this.state.listVideos);
      },
    );
  }
  renderVideos() {
    const videos = this.props.videos.entries || [];
    return videos.map((video, i) => (
      <span
        key={video.id}
        tabIndex={i + 1}
        role="button"
        onKeyPress={e =>
          this.addVideoHistory(e, {
            id: video.id,
            title: video.title,
            image: video.images[0].url,
          })}
        onClick={e =>
          this.addVideoHistory(e, {
            id: video.id,
            title: video.title,
            image: video.images[0].url,
          })}
      >
        <Video image={video.images[0].url} title={video.title} />
      </span>
    ));
  }

  render() {
    return (
      <div className="all-videos">
        <p className="legend col-xs-12">
          Use Tab or Tab + Shift to navigate between videos and arrows to move
          to left or right
        </p>
        <Loading isShow={this.props.loading ? 'hide' : ''} />
        {this.renderVideos()}
      </div>
    );
  }
}
export default connect(
  state => ({
    videos: state.video.videos,
    loading: state.video.loading,
    listVideo: state.video.listVideo,
  }),
  { getVideos, salveLocalStorage, redirect },
)(Videos);
