import React from 'react';
import Player from './Player';

const Modal = props => (
  <span>
    <input className="modal-state" id={props.index} type="checkbox" />
    <div className="modal">
      <label className="modal-bg" htmlFor={props.index} />
      <div className="modal-inner">
        <label className="modal-close" htmlFor={props.index} />
        <h2>{props.title}</h2>
        <div>
          <Player videoUrl={props.videoUrl} />
        </div>
      </div>
    </div>
  </span>
);

export default Modal;
