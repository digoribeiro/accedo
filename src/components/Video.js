import React from 'react';

const Video = props => (
  <div className={props.className || 'video col-md-2'}>
    <img
      src={props.image}
      className="video-cover img-responsive"
      alt={`Description ${props.title}`}
    />
    <h3 className="video-title reticence">{props.title}</h3>
  </div>
);

export default Video;
