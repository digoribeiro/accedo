import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getVideos } from '../actions/videos';
import Player from '../components/Player';

class SingleVideo extends Component {
  componentWillMount() {
    if (!this.props.videos.entries) {
      this.props.getVideos();
    }
  }
  renderVideo() {
    const video = this.props.videos.entries || [];
    const params = this.props.match.params.videoId;
    const obj = video.find(item => item.id === params);

    return obj && <Player title={obj.title} videoUrl={obj.contents[0].url} />;
  }

  render() {
    return <div className="player">{this.renderVideo()}</div>;
  }
}

export default connect(
  state => ({
    videos: state.video.videos
  }),
  {
    getVideos
  }
)(SingleVideo);
