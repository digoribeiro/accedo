import React from 'react';
import Videos from '../components/Videos';

const Home = () => (
  <div className="row">
    <Videos />
  </div>
);

export default Home;
