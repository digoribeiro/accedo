import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import videoReducer from './videoReducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  video: videoReducer,
});

export default rootReducer;
