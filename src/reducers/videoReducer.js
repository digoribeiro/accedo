const INITIAL_STATE = {
  videos: {},
  loading: true,
  listVideo: JSON.parse(localStorage.getItem('listWatchedVideos')) || [],
  showVideoHistory: true,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'VIDEOS_FETCHED_FULFILLED':
      return {
        ...state,
        videos: action.payload.data,
        loading: true,
        listVideo: state.listVideo,
      };
    case 'VIDEOS_FETCHED_PENDING':
      return { ...state, loading: false };
    case 'LIST_CHANGED':
      localStorage.setItem(
        'listWatchedVideos',
        JSON.stringify(action.listVideo),
      );
      return { ...state, listVideo: action.listVideo };
    case 'HISTORY_SHOW': {
      const show = state.showVideoHistory;
      return { ...state, showVideoHistory: !show };
    }
    default:
      return state;
  }
};
