
# Front-end - Accedo
## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:

* Git - [Download & Install Git](https://git-scm.com/downloads).
* Node.js - [Download & Install Node.js](https://nodejs.org/en/download/)

### Cloning The GitHub Repository

```bash
$ git clone https://digoribeiro@bitbucket.org/digoribeiro/accedo.git
```

To install the dependencies, run this in the application folder from the command-line:

```bash
$ cd accedo && npm install
```

## Running Front

Run Front using npm:

```bash
$ npm start
```

Front-end should run on port 3000 [http://localhost:3000](http://localhost:3000)

## Folders and Files

```sh
├── images
├── src
│   ├── actions
│   ├── components
│   ├── containers
│   ├── reducers
│   ├── services
│   ├── styles
│   │   ├── base
│   │   ├── compoents
│   │   ├── generic
│   │   ├── objects
│   │   ├── settings
│   │   ├── tools
│   │   ├── trumps
│   │   └── style.styl
│   ├── views
│   ├── app.js
│   ├── configureStore.js
│   ├── constants.js
│   ├── history.js
│   ├── root.js
│   └── routes.js
├── tests
│   ├── specs
│   │   └── views
│   ├── utils
│   │   └── helper.js
│   └── setup.js
├── webpack
│   ├── base.js
│   ├── dev.js
│   └── prod.js
├── .babelrc
├── .editorconfig
├── .eslintrc
├── .gitignore
├── .npmrc
├── CONTRIBUTING.md
├── LICENSE.md
├── README.md
├── index.html
└── package.json
```

## Commands

* **npm start**: start a server on [http://localhost:3000](http://localhost:3000).
* **npm test**: run your tests in a single-run mode.
* **npm run test:tdd**: run and keep watching your test files.
* **npm run lint**: lint all files searching for errors.
* **npm run lint:fix**: fix automaticaly some lint errors.
* **npm run ci**: run tests and lint.
* **npm build**: build the project at `dist`.

## Demos

**API:** [http://accedo-teste-api-tv.umbler.net/videos](http://accedo-teste-api-tv.umbler.net/videos)

**FRONT:** [http://accedo-teste-tv.umbler.net/](http://accedo-teste-tv.umbler.net/)

## Stack

- [React](https://facebook.github.io/react/)
- [React Router](https://github.com/ReactTraining/react-router)
- [Redux](http://redux.js.org/docs/introduction/)
- [Babel 6](https://babeljs.io/) - Javascript Compiler.
- [Webpack](https://webpack.github.io/) - Javascript module bundler.
- [Webpack Dashboard](https://github.com/FormidableLabs/webpack-dashboard)
- [Eslint](http://eslint.org/) - The pluggable linting utility for JavaScript and JSX.
- [Husky](https://github.com/typicode/husky) - Git hooks made easy.
- [Mocha](https://mochajs.org/) - JavaScript test framework.
- [Chai](http://chaijs.com/) - BDD / TDD assertion library.
- [Sinon](http://sinonjs.org/) - Standalone test spies, stubs and mocks for JavaScript.
- [Nyc](https://github.com/istanbuljs/nyc) - Istanbul command line interface.
- [Enzyme](http://airbnb.io/enzyme/) - JavaScript Testing utility for React.
- [JSDOM](https://github.com/tmpvar/jsdom) - A JavaScript implementation of the WHATWG DOM and HTML standards.
- [Stylus](http://stylus-lang.com/) - Preprocessor CSS
- [PostCSS](http://postcss.org/) - A tool for transforming CSS with JavaScript

#Design Question

##Describe the data you would capture as part of this service.
 - It would collect information such as: user, access region, device type, browser, date and time of navigation.

##How would you make this service more efficient?
 - With some user information, it would redirect it to the server closest to it, for example, it would also put the maximum cache in the application and the servers and optimization on the front whenever possible.

##Imagine that the JSON file that provides all the videos has a 'rate limit' that blocks you to access that file more than 5 times per hour that file. Please describe how you would bypass that limitation given that your application will be used by thousands of users.
 - I would work with PWA in the Front End. By caching the application and using a NoSql database, elasticsearch for example, and redirect the user to it whenever possible.

##Once the feature is complete, how would you know that it's ready for go-live?
- Testing, doing all kinds of automated tests, performace and testing with users.

##How would you determine if this feature is successful?
- With the information I got from the first question, I would gather all the information and analyze. To know how much the number of hits and how many people have managed to use the application. Depending on the situation, I would try to do a survey with the users to see if they are actually more satisfied.
